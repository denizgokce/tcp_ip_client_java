import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import java.util.UUID;

/**
 * Created by Deniz Gokce on 18.8.2015.
 */

public class Main {

    public static SocketAddress socketAddress = new InetSocketAddress("127.0.0.1", 5000);
    public static Socket clientSocket = new Socket();
    public static UUID MyID = null;
    public static Gson gson = new GsonBuilder().create();


    public static void main(String[] args) throws IOException, InterruptedException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Client PersistendClient = new Client();

        System.out.print("Enter Your Username : ");
        PersistendClient.setUsername(br.readLine());
        System.out.print("Enter Your Room : ");
        PersistendClient.setGroupName(br.readLine());
        System.out.print("Enter Your Rank : ");
        PersistendClient.setActivityRank(br.readLine());

        ConnectLoop(PersistendClient);


        new Thread(new SendLoop()).start();
        new Thread(new RecieveLoop()).start();

        System.in.read();
    }

    public static class SendLoop implements Runnable {

        @Override
        public void run() {
            String message = "";
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                OutputStream os = clientSocket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);

                while ((message = br.readLine()) != null) {
                    Message msg = new Message();
                    msg.setConnectionID(MyID);
                    msg.setMessageText(message);

                    bw.write(gson.toJson(msg).toString());
                    bw.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class RecieveLoop implements Runnable {

        @Override
        public void run() {
            try {
                int red = -1;
                byte[] buffer = new byte[1024]; // a read buffer of 5KiB
                byte[] _buffer;
                String txt;
                while ((red = clientSocket.getInputStream().read(buffer)) > -1) {
                    _buffer = new byte[red];
                    System.arraycopy(buffer, 0, _buffer, 0, red);
                    txt = new String(_buffer, "UTF-8"); // assumption that client sends data UTF-8 encoded

                    Message msg = gson.fromJson(txt, Message.class);

                    if (msg.getInit()) {
                        MyID = msg.getConnectionID();
                        System.out.println(msg.getMessageText());
                    } else {
                        System.out.println(msg.getMessageText());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void ConnectLoop(Client PersistendClient) throws IOException, InterruptedException {

        int attempts = 0;

        while (!clientSocket.isConnected()) {

            try {
                attempts++;
                clientSocket.connect(socketAddress);
            } catch (Exception ex) {
                //Runtime.getRuntime().exec("cls");
                System.out.println("Conneciton attempts : " + attempts);
            }
            Thread.sleep(1000);
        }


        Message first = new Message();
        first.setInit(true);
        first.setConnectionID(new UUID(0L, 0L));
        first.setMessageText(gson.toJson(PersistendClient).toString());


        String firstMessage = gson.toJson(first).toString();

        OutputStream os = clientSocket.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write(firstMessage);
        bw.flush();

        System.out.println("Connection Established...\n");
    }

}
