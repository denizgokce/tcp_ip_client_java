
import java.util.UUID;

/**
 * Created by Deniz Gokce on 18.8.2015.
 */
public class Message {

    private Boolean Init;
    private UUID ConnectionID;
    private String MessageText;

    public UUID getConnectionID() {
        return ConnectionID;
    }

    public void setConnectionID(UUID connectionID) {
        ConnectionID = connectionID;
    }

    public String getMessageText() {
        return MessageText;
    }

    public void setMessageText(String messageText) {
        MessageText = messageText;
    }

    public Boolean getInit() {
        return Init;
    }

    public void setInit(Boolean init) {
        Init = init;
    }
}
