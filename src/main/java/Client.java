

/**
 * Created by Deniz Gokce on 18.8.2015.
 */
public class Client {
    private String Username;
    private String groupName;
    private String ActivityRank;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getActivityRank() {
        return ActivityRank;
    }

    public void setActivityRank(String activityRank) {
        ActivityRank = activityRank;
    }
}
